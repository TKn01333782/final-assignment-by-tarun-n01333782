﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NewPage.aspx.cs" Inherits="Final_Project.NewPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
     <h3 runat="server" id="add_page">ADD NEW PAGE HERE :- </h3>
        <br />
        <br />

        <asp:SqlDataSource runat="server" id="add_new_page" ConnectionString="<%$ ConnectionStrings:final_sql_con %>"></asp:SqlDataSource>
        <asp:SqlDataSource runat="server" id="page_add" connectionString="<%$ ConnectionStrings:final_sql_con %>"></asp:SqlDataSource>
    
    <div class="inputrow">
        <label>Page Title :</label>
        <asp:TextBox ID="page_title" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ControlToValidate="page_title" ErrorMessage="Enter a text"></asp:RequiredFieldValidator>
        </div>
    
    <div class="inputrow">
        <label>Pages Content :</label>
        <asp:TextBox ID="page_content" runat="server" Height="27px" Width="226px"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ControlToValidate="page_content" ErrorMessage="Enter a valid number"></asp:RequiredFieldValidator>
        </div>
   
    <asp:button runat="server" OnClick="add_a_page" Text="Click to Add" />
    <div runat="server" id="debug" class="querybox"></div>

</asp:Content>
