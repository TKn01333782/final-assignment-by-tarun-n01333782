﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewPage.aspx.cs" Inherits="Final_Project.ViewPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h3 runat="server" id="nextpagetitle"></h3>
    <p  runat="server" id="nextpagecontent"></p>
    <asp:SqlDataSource ID="page_select" runat="server" ConnectionString="<%$ ConnectionStrings:final_sql_con %>"></asp:SqlDataSource>
   
      <a href="Editpages.aspx?pageid=<% Response.Write(this.pageid);%>">Edit</a>
    <br />
    <br />

    <asp:SqlDataSource runat="server" id="delete_page" ConnectionString="<%$ ConnectionStrings:final_sql_con %>"></asp:SqlDataSource>
    <asp:Button runat="server" Text="Delete" OnClick="removePage" />
</asp:Content>
