﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace Final_Project
{
    public partial class ListPages : Page
    {
        private String sqlline = "Select pageid,pagetitle,pagecontent from Pages";


        protected DataView ListPages_Manual_Bind(SqlDataSource src)
        {
            DataTable Newtable;
            DataView Newview;
            Newtable = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in Newtable.Rows)
            {
               row["pagetitle"] = "<a href=\"Viewpage.aspx?pageid="+row["pageid"]+"\">"+row["pagetitle"]+"</a>";
               // row["pagetitle"] = "<a href=\"ListPages.aspx?pagetitle="+ row["pagetitle"] + "\">" + row["pagetitle"]+"</a>";

            }
            Newview = Newtable.DefaultView;
            return Newview;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            page_select.SelectCommand = sqlline;
            Pages_query.InnerHtml = sqlline;
            page_list.DataSource = ListPages_Manual_Bind(page_select);
            page_list.DataBind();


            ButtonColumn delete_page = new ButtonColumn();
            delete_page.HeaderText = "Delete";
            delete_page.Text = "Delete";
            delete_page.ButtonType = ButtonColumnType.PushButton;
            delete_page.CommandName = "Del_Page";
            page_list.Columns.Add(delete_page);

            page_list.DataBind();

        }
        protected void Page_ItemCommand(object sender, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Del_Page")
            {
                int pageid = Convert.ToInt32(e.Item.Cells[1].Text);
                Del_page(pageid);
               
            }

        }

        protected void Del_page(int pageid)
        {
            string basequery = "DELETE FROM Pages WHERE pageid=" + pageid.ToString();
            delete_page.DeleteCommand = basequery;
            delete_page.Delete();
            Pages_query.InnerHtml = basequery;
        }

    }
}