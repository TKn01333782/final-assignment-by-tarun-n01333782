﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Final_Project
{

    public partial class Editpages : System.Web.UI.Page
    {
       
        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        //page_title.Text = "info from page "+pageid;
        //page_content.Text = "info from page " + pageid;
        protected override void OnPreRender(EventArgs e)
        {
            string query = "select * from pages where pageid =" + pageid;
            page_select.SelectCommand = query;

            DataTable pageview = ((DataView)page_select.Select(DataSourceSelectArguments.Empty)).ToTable();
            DataRow pagerowview = pageview.Rows[0];
            page_title.Text = pagerowview["pagetitle"].ToString();
            page_content.Text = pagerowview["pagecontent"].ToString();
        }

        protected void Edit_Page(object sender, EventArgs e)
        {
            //debug.InnerHtml = "I am trying to edit page" + pageid;
            //build an update SQL statement with the values from page_title and page_content

            string title = page_title.Text;
            string content = page_content.Text;

            string query = "update Pages set pagetitle='"+title+"', pagecontent='"+content+"' where pageid =" + pageid;
            debug.InnerHtml = query;

            edit_pages.UpdateCommand = query;
            edit_pages.Update();

            //use your update sqldatasource to run an updatecommand and the method of update()


        }
        


    }
}
    
