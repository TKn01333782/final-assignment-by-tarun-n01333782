﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Final_Project.usercontrol
{
    public partial class pagemenu : System.Web.UI.UserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack) { 
                return;
            }
            page_menu.SelectCommand = "select * from pages";
            DataView myview = (DataView)page_menu.Select(DataSourceSelectArguments.Empty);
            string menuitems = "";
            foreach (DataRowView row in myview)
            {
                string id = row["pageid"].ToString();
                string title = row["pagetitle"].ToString();

                menuitems += "<li><a href=\"Viewpage.aspx?pageid=" + id + "\">" + title + "</a></li>"; ;
                
            }


            // row["pagetitle"] = "<a href=\"Viewpage.aspx?pageid=" + row["pageid"] + "\">" + row["pagetitle"] + "</a>";
        

            menu_container.InnerHtml += menuitems;


        }
    }
}