﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListPages.aspx.cs" Inherits="Final_Project.ListPages" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <br />

    <div id="new_page" runat="server">
        <a href="NewPage">Add New Page</a>
     
    </div>
    <br />
    <asp:SqlDataSource runat="server" ID="page_select" connectionString="<%$ connectionStrings:final_sql_con %>"></asp:SqlDataSource>
    

    <div id="Pages_query" class="querybox" runat="server"></div>
    <br />
    
    <asp:DataGrid runat="server" ID="page_list" Height="142px" Width="382px" OnItemCommand="Page_ItemCommand"></asp:DataGrid>


    
    <asp:SqlDataSource runat="server" id="delete_page" ConnectionString="<%$ ConnectionStrings:final_sql_con %>">
    </asp:SqlDataSource>
    
</asp:Content>
