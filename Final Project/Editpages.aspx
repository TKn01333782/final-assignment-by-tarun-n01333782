﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Editpages.aspx.cs" Inherits="Final_Project.Editpages" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <h3 runat="server" id="pages_name">Edit Pages</h3>
        <asp:SqlDataSource runat="server" id="edit_pages" ConnectionString="<%$ ConnectionStrings:final_sql_con %>"></asp:SqlDataSource>
        <asp:SqlDataSource runat="server" id="page_select" connectionString="<%$ ConnectionStrings:final_sql_con %>"></asp:SqlDataSource>
    
    <div class="inputrow">
           <label>Page Title: </label>
        <asp:TextBox ID="page_title" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ControlToValidate="page_title" ErrorMessage="Enter a valid text"></asp:RequiredFieldValidator>
    </div>
    
    <div class="inputrow">
           <label>Pages Content:</label>
        <asp:TextBox ID="page_content" runat="server" Height="27px" Width="214px"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ControlToValidate="page_content" ErrorMessage="Enter a valid text"></asp:RequiredFieldValidator>
        </div>
   <asp:button runat="server" OnClick="Edit_Page" Text="Edit" />
     
    <div runat="server" id="debug" class="querybox"></div>

     
</asp:Content>
