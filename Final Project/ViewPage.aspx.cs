﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Final_Project
{
   
    public partial class ViewPage : System.Web.UI.Page
    {
        public string pageid
        {
            get { return Request.QueryString["pageid"]; }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            string sqlquery = "select * from Pages where pageid=" + pageid;

            page_select.SelectCommand = sqlquery;

            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);
            DataRowView pagerowview = pageview[0];

            nextpagetitle.InnerHtml = pagerowview["pagetitle"].ToString();
            nextpagecontent.InnerHtml = pagerowview["pagecontent"].ToString();
        }


        protected void removePage(object sender, EventArgs e)
        {
            string deletequery = "delete from Pages where pageid= " + pageid;
            delete_page.DeleteCommand = deletequery;
            delete_page.Delete();


        }
               
    }
    
}